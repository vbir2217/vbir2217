package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticPosition;
import salariati.exception.NonValidEmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepository;
import salariati.repository.mock.EmployeeRepositoryMock;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AddEmployeeTest {

    private EmployeeRepository employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeRepositoryMock();
        controller = new EmployeeController(employeeRepository);
        employeeValidator = new EmployeeValidator();
    }

//    @Test
//    public void testRepositoryMock() {
//        assertFalse(controller.getEmployeesList().isEmpty());
//        assertEquals(6, controller.getEmployeesList().size());
//    }
//
//    public void testAddNewEmployee() {
//        Employee newEmployee = new Employee("Gigel", "ValidLastName", "1910509055057", DidacticPosition.LECTURER, 3000);
//        assertTrue(employeeValidator.isValid(newEmployee));
//        try {
//            controller.addEmployee(newEmployee);
//            assertEquals(6, controller.getEmployeesList().size());
//            assertEquals(newEmployee, controller.getEmployeesList().get(controller.getEmployeesList().size() - 1));
//        } catch (NonValidEmployeeException e) {
//            e.printStackTrace();
//        }
//    }

}
