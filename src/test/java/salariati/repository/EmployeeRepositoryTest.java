package salariati.repository;

import org.junit.After;
import org.junit.Test;
import salariati.exception.NonValidEmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepository;
import salariati.repository.mock.EmployeeRepositoryMock;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static salariati.enumeration.DidacticPosition.ASSISTANT_PROFESSOR;
import static salariati.enumeration.DidacticPosition.ASSOCIATE_PROFESSOR;
import static salariati.enumeration.DidacticPosition.LECTURER;
import static salariati.enumeration.DidacticPosition.PROFESSOR;

public class EmployeeRepositoryTest {

    private static final Employee EMPLOYEE_1 = new Employee("Dan", "Alb", "1234567891234", ASSISTANT_PROFESSOR, 5600);
    private static final Employee EMPLOYEE_2 = new Employee("Mihaela", "Suciu", "2234567891234", PROFESSOR, 5600);
    private static final Employee EMPLOYEE_3 = new Employee("Gabriel", "Muresan", "1234567891294", LECTURER, 5600);

    private EmployeeRepository employeeRepository = new EmployeeRepositoryMock();

    @After
    public void tearDown() {
        employeeRepository.deleteAll();
    }

    @Test
    public void F02_TC01() {
        try {
            addThreeEmployees();
        } catch (NonValidEmployeeException e) {
            e.printStackTrace();
        }
        try {
            employeeRepository.modificaFunctieAnagajat(null, LECTURER);
            fail();
        } catch (NonValidEmployeeException e) {
            assertEquals(e.getMessage(), "Cnp-ul nu poate fi neprecizat.");
        }
    }

    @Test
    public void F02_TC02() {
        try {
            addThreeEmployees();
        } catch (NonValidEmployeeException e) {
            e.printStackTrace();
        }
        try {
            employeeRepository.modificaFunctieAnagajat("", LECTURER);
            fail();
        } catch (NonValidEmployeeException e) {
            assertEquals(e.getMessage(), "Cnp-ul nu poate fi neprecizat.");
        }
    }

    @Test
    public void F02_TC03() {
        try {
            addTwoEmployees();
        } catch (NonValidEmployeeException e) {
            e.printStackTrace();
        }
        try {
            employeeRepository.modificaFunctieAnagajat("1234567891234", null);
            fail();
        } catch (NonValidEmployeeException e) {
            assertEquals(e.getMessage(), "Functia didactica trebuie introdusa.");
        }
    }

    @Test
    public void F02_TC04() {
        final String cnp = "1234567891234";
        try {
            addTwoEmployees();
        } catch (NonValidEmployeeException e) {
            e.printStackTrace();
        }
        try {
            Optional<Employee> oldEmployeeOptional = employeeRepository.getEmployeeByCnp(cnp);
            assertTrue(oldEmployeeOptional.isPresent());
            Employee oldEmployee = oldEmployeeOptional.get();
            assertEquals(oldEmployee.getPosition(), ASSISTANT_PROFESSOR);

            employeeRepository.modificaFunctieAnagajat(cnp, LECTURER);

            Optional<Employee> newEmployeeOptional = employeeRepository.getEmployeeByCnp(cnp);
            assertTrue(newEmployeeOptional.isPresent());
            Employee newEmployee = newEmployeeOptional.get();
            assertEquals(newEmployee.getPosition(), LECTURER);
        } catch (NonValidEmployeeException e) {
            fail();
        }
    }

    @Test
    public void F02_TC05() {
        try {
            assertEquals(employeeRepository.getEmployeesList().size(), 0);
            employeeRepository.modificaFunctieAnagajat("1234567891234", LECTURER);
            assertEquals(employeeRepository.getEmployeesList().size(), 0);
        } catch (NonValidEmployeeException e) {
            fail();
        }
    }

    @Test
    public void F02_TC06() {
        try {
            addTwoEmployees();
        } catch (NonValidEmployeeException e) {
            e.printStackTrace();
        }
        try {
            List<Employee> oldEmployeesList = employeeRepository.getEmployeesList();
            assertEquals(oldEmployeesList.size(), 2);

            employeeRepository.modificaFunctieAnagajat("2234567891235", ASSOCIATE_PROFESSOR);

            List<Employee> newEmployeesList = employeeRepository.getEmployeesList();
            assertEquals(newEmployeesList.size(), 2);
            assertEquals(newEmployeesList.get(0).getPosition(), oldEmployeesList.get(0).getPosition());
            assertEquals(newEmployeesList.get(1).getPosition(), oldEmployeesList.get(1).getPosition());
        } catch (NonValidEmployeeException e) {
            fail();
        }
    }

    private void addThreeEmployees() throws NonValidEmployeeException {
        employeeRepository.addEmployee(EMPLOYEE_1);
        employeeRepository.addEmployee(EMPLOYEE_2);
        employeeRepository.addEmployee(EMPLOYEE_3);
    }

    private void addTwoEmployees() throws NonValidEmployeeException {
        employeeRepository.addEmployee(EMPLOYEE_1);
        employeeRepository.addEmployee(EMPLOYEE_2);
    }

    private void addOneEmployees() throws NonValidEmployeeException {
        employeeRepository.addEmployee(EMPLOYEE_1);
    }
}
