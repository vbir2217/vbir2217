package salariati.controller;

import org.junit.Before;
import org.junit.Test;
import salariati.exception.NonValidEmployeeException;
import salariati.model.Employee;
import salariati.model.EmployeeBuilder;
import salariati.repository.interfaces.EmployeeRepository;
import salariati.repository.mock.EmployeeRepositoryMock;

import java.util.List;

import static java.util.stream.IntStream.range;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static salariati.enumeration.DidacticPosition.ASSISTANT_PROFESSOR;
import static salariati.enumeration.DidacticPosition.ASSOCIATE_PROFESSOR;
import static salariati.enumeration.DidacticPosition.LECTURER;
import static salariati.enumeration.DidacticPosition.PROFESSOR;

public class EmployeeControllerTest {

    private EmployeeRepository employeeRepository = new EmployeeRepositoryMock();
    private EmployeeController employeeController = new EmployeeController(employeeRepository);

    @Before
    public void setUp() {
        employeeRepository.deleteAll();
    }

    //  ECP

    //  TC1_EC
    @Test
    public void addEmployeeValidDataTest() {
        Employee employee = new EmployeeBuilder()
                .lastName("Moldovan")
                .firstName("Marian")
                .cnp("1920304231235")
                .didacticPosition(LECTURER)
                .salary(2300.00)
                .build();

        try {
            employeeController.addEmployee(employee);
            List<Employee> employeesList = employeeController.getEmployeesList();
            assertEquals(employeesList.size(), 1);
            Employee actual = employeesList.get(0);
            assertEquals(actual, employee);
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }

    //  TC2_EC
    @Test
    public void addEmployeeSalaryAndNameNotValidTest() {
        StringBuilder nameBuilder = new StringBuilder();
        range(0, 300).forEach(i -> nameBuilder.append("A"));
        Employee employee = new EmployeeBuilder()
                .lastName(nameBuilder.toString())
                .firstName("Marian")
                .cnp("1821304231235")
                .didacticPosition(ASSOCIATE_PROFESSOR)
                .salary(-1105.00)
                .build();

        try {
            employeeController.addEmployee(employee);
            fail();
        } catch (NonValidEmployeeException e) {
            assertEquals(e.getMessage(),
                    "\nNumele poate avea o lungime de maxim 255\nSalariul trebuie sa fie un numar pozitiv diferit de 0 si maxim 20000.");
        }
    }

    //  TC3_EC
    @Test
    public void addEmployeeBlankNameAndZeroSalaryTest() {
        Employee employee = new EmployeeBuilder()
                .lastName("")
                .firstName("Dorin")
                .cnp("1811304231235")
                .didacticPosition(LECTURER)
                .salary(0)
                .build();

        try {
            employeeController.addEmployee(employee);
            fail();
        } catch (NonValidEmployeeException e) {
            assertEquals(e.getMessage(),
                    "\nNumele nu poate sa fie vid.\nSalariul trebuie sa fie un numar pozitiv diferit de 0 si maxim 20000.");
        }
    }

    //  TC4_EC
    @Test
    public void addEmployeeNegativeSalaryTest() {
        Employee employee = new EmployeeBuilder()
                .lastName("Danciu")
                .firstName("Marin")
                .cnp("1811305231235")
                .didacticPosition(ASSISTANT_PROFESSOR)
                .salary(-123.00)
                .build();

        try {
            employeeController.addEmployee(employee);
            fail();
        } catch (NonValidEmployeeException e) {
            assertEquals(e.getMessage(),
                    "\nSalariul trebuie sa fie un numar pozitiv diferit de 0 si maxim 20000.");
        }
    }

    //  TC5_EC
    @Test
    public void addEmployeeSalaryExceedsMaxLimit() {
        Employee employee = new EmployeeBuilder()
                .lastName("Socol")
                .firstName("Alexandra")
                .cnp("2815052331235")
                .didacticPosition(PROFESSOR)
                .salary(260000.00)
                .build();

        try {
            employeeController.addEmployee(employee);
            fail();
        } catch (NonValidEmployeeException e) {
            assertEquals(e.getMessage(),
                    "\nSalariul trebuie sa fie un numar pozitiv diferit de 0 si maxim 20000.");
        }
    }

    //  TC6_EC
    @Test
    public void addEmployeeNameExceedsLimit() {
        StringBuilder nameBuilder = new StringBuilder();
        range(0, 280).forEach(i -> nameBuilder.append("A"));
        Employee employee = new EmployeeBuilder()
                .lastName(nameBuilder.toString())
                .firstName("Tudor")
                .cnp("1821304231235")
                .didacticPosition(ASSOCIATE_PROFESSOR)
                .salary(1111.0)
                .build();

        try {
            employeeController.addEmployee(employee);
            fail();
        } catch (NonValidEmployeeException e) {
            assertEquals(e.getMessage(),
                    "\nNumele poate avea o lungime de maxim 255");
        }
    }

    // BVA

    //  TC1_BVA <=> TC3_EC

    //  TC2_BVA
    @Test
    public void addEmployeeLowerBoundSalary0_01Test() {
        Employee employee = new EmployeeBuilder()
                .lastName("Mihai")
                .firstName("Marin")
                .cnp("1920304231235")
                .didacticPosition(LECTURER)
                .salary(0.01)
                .build();

        try {
            employeeController.addEmployee(employee);
            List<Employee> employeesList = employeeController.getEmployeesList();
            assertEquals(employeesList.size(), 1);
            Employee actual = employeesList.get(0);
            assertEquals(actual, employee);
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }

    //  TC3_BVA
    @Test
    public void addEmployeeLowerBoundSalary0_02Test() {
        Employee employee = new EmployeeBuilder()
                .lastName("Mihai")
                .firstName("Marin")
                .cnp("1920304231235")
                .didacticPosition(LECTURER)
                .salary(0.02)
                .build();

        try {
            employeeController.addEmployee(employee);
            List<Employee> employeesList = employeeController.getEmployeesList();
            assertEquals(employeesList.size(), 1);
            Employee actual = employeesList.get(0);
            assertEquals(actual, employee);
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }

    //  TC4_BVA
    @Test
    public void addEmployeeUpperBoundSalary19999_99Test() {
        Employee employee = new EmployeeBuilder()
                .lastName("Lazar")
                .firstName("Elena")
                .cnp("2720304231235")
                .didacticPosition(ASSOCIATE_PROFESSOR)
                .salary(19999.99)
                .build();

        try {
            employeeController.addEmployee(employee);
            List<Employee> employeesList = employeeController.getEmployeesList();
            assertEquals(employeesList.size(), 1);
            Employee actual = employeesList.get(0);
            assertEquals(actual, employee);
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }

    //  TC5_BVA
    @Test
    public void addEmployeeUpperBoundSalary20000_00Test() {
        Employee employee = new EmployeeBuilder()
                .lastName("Lazar")
                .firstName("Mihnea")
                .cnp("2720304231235")
                .didacticPosition(PROFESSOR)
                .salary(20000.00)
                .build();

        try {
            employeeController.addEmployee(employee);
            List<Employee> employeesList = employeeController.getEmployeesList();
            assertEquals(employeesList.size(), 1);
            Employee actual = employeesList.get(0);
            assertEquals(actual, employee);
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }

    //  TC6_BVA
    @Test
    public void addEmployeeUpperBoundSalary20000_01Test() {
        Employee employee = new EmployeeBuilder()
                .lastName("Duca")
                .firstName("Cristina")
                .cnp("2720904231235")
                .didacticPosition(ASSISTANT_PROFESSOR)
                .salary(20000.01)
                .build();

        try {
            employeeController.addEmployee(employee);
            fail();
        } catch (NonValidEmployeeException e) {
            assertEquals(e.getMessage(),
                    "\nSalariul trebuie sa fie un numar pozitiv diferit de 0 si maxim 20000.");
        }
    }

    //  TC7_BVA <=> TC3_EC

    //  TC8_BVA
    @Test
    public void addEmployeeLowerBoundName_A_Test() {
        Employee employee = new EmployeeBuilder()
                .lastName("A")
                .firstName("Dorin")
                .cnp("1920304231235")
                .didacticPosition(ASSOCIATE_PROFESSOR)
                .salary(2000.01)
                .build();

        try {
            employeeController.addEmployee(employee);
            List<Employee> employeesList = employeeController.getEmployeesList();
            assertEquals(employeesList.size(), 1);
            Employee actual = employeesList.get(0);
            assertEquals(actual, employee);
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }

    //  TC9_BVA
    @Test
    public void addEmployeeLowerBoundName_AA_Test() {
        Employee employee = new EmployeeBuilder()
                .lastName("AA")
                .firstName("Dorin")
                .cnp("1920304231235")
                .didacticPosition(LECTURER)
                .salary(1999.14)
                .build();

        try {
            employeeController.addEmployee(employee);
            List<Employee> employeesList = employeeController.getEmployeesList();
            assertEquals(employeesList.size(), 1);
            Employee actual = employeesList.get(0);
            assertEquals(actual, employee);
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }

    //  TC10_BVA
    @Test
    public void addEmployeeUpperBoundNameAA___A_254Test() {
        StringBuilder nameBuilder = new StringBuilder();
        range(0, 254).forEach(i -> nameBuilder.append("A"));
        Employee employee = new EmployeeBuilder()
                .lastName(nameBuilder.toString())
                .firstName("B")
                .cnp("1920304231235")
                .didacticPosition(ASSISTANT_PROFESSOR)
                .salary(2323.54)
                .build();

        try {
            employeeController.addEmployee(employee);
            List<Employee> employeesList = employeeController.getEmployeesList();
            assertEquals(employeesList.size(), 1);
            Employee actual = employeesList.get(0);
            assertEquals(actual, employee);
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }

    //  TC11_BVA
    @Test
    public void addEmployeeUpperBoundNameAA___A_255Test() {
        StringBuilder nameBuilder = new StringBuilder();
        range(0, 255).forEach(i -> nameBuilder.append("A"));
        Employee employee = new EmployeeBuilder()
                .lastName(nameBuilder.toString())
                .firstName("B")
                .cnp("1920304231235")
                .didacticPosition(ASSISTANT_PROFESSOR)
                .salary(2323.54)
                .build();

        try {
            employeeController.addEmployee(employee);
            List<Employee> employeesList = employeeController.getEmployeesList();
            assertEquals(employeesList.size(), 1);
            Employee actual = employeesList.get(0);
            assertEquals(actual, employee);
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }

    //  TC12_BVA
    @Test
    public void addEmployeeUpperBoundNameAA___A_256Test() {
        StringBuilder nameBuilder = new StringBuilder();
        range(0, 256).forEach(i -> nameBuilder.append("A"));
        Employee employee = new EmployeeBuilder()
                .lastName(nameBuilder.toString())
                .firstName("B")
                .cnp("1920304231235")
                .didacticPosition(ASSISTANT_PROFESSOR)
                .salary(2323.54)
                .build();

        try {
            employeeController.addEmployee(employee);
            fail();
        } catch (NonValidEmployeeException e) {
            assertEquals(e.getMessage(),
                    "\nNumele poate avea o lungime de maxim 255");
        }
    }

    @Test
    public void testF03_SortByAgeAscending() {
        try {
            employeeRepository.addEmployee(new Employee("firstName1", "lastName1", "1920603891234", LECTURER, 2.2));
            employeeRepository.addEmployee(new Employee("firstName2", "lastName2", "1911202891234", ASSISTANT_PROFESSOR, 22.2));
            employeeRepository.addEmployee(new Employee("firstName3", "lastName3", "19011121234", ASSOCIATE_PROFESSOR, 122.2));

            List<Employee> employeesSortedByAgeAscending = employeeRepository.getEmployeesSortedByAgeAscending();

            assertEquals(employeesSortedByAgeAscending.size(), 3);
            Employee employee1 = employeesSortedByAgeAscending.get(0);
            assertNotNull(employee1);
            assertEquals(employee1.getFirstName(), "firstName1");

            Employee employee2 = employeesSortedByAgeAscending.get(1);
            assertNotNull(employee2);
            assertEquals(employee2.getFirstName(), "firstName2");

            Employee employee3 = employeesSortedByAgeAscending.get(2);
            assertNotNull(employee3);
            assertEquals(employee3.getFirstName(), "firstName3");
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testF03_SortBySalaryDescending() {
        try {
            employeeRepository.addEmployee(new Employee("firstName1", "lastName1", "1920603891234", LECTURER, 2.2));
            employeeRepository.addEmployee(new Employee("firstName2", "lastName2", "1911202891234", ASSISTANT_PROFESSOR, 22.2));
            employeeRepository.addEmployee(new Employee("firstName3", "lastName3", "19011121234", ASSOCIATE_PROFESSOR, 122.2));

            List<Employee> employeesSortedBySalaryDescending = employeeRepository.getEmployeesSortedBySalaryDescending();

            assertEquals(employeesSortedBySalaryDescending.size(), 3);
            Employee employee1 = employeesSortedBySalaryDescending.get(0);
            assertNotNull(employee1);
            assertEquals(employee1.getFirstName(), "firstName3");

            Employee employee2 = employeesSortedBySalaryDescending.get(1);
            assertNotNull(employee2);
            assertEquals(employee2.getFirstName(), "firstName2");

            Employee employee3 = employeesSortedBySalaryDescending.get(2);
            assertNotNull(employee3);
            assertEquals(employee3.getFirstName(), "firstName1");
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }
}