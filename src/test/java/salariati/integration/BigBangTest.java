package salariati.integration;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.exception.NonValidEmployeeException;
import salariati.model.Employee;
import salariati.model.EmployeeBuilder;
import salariati.repository.interfaces.EmployeeRepository;
import salariati.repository.mock.EmployeeRepositoryMock;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static salariati.enumeration.DidacticPosition.ASSISTANT_PROFESSOR;
import static salariati.enumeration.DidacticPosition.ASSOCIATE_PROFESSOR;
import static salariati.enumeration.DidacticPosition.LECTURER;
import static salariati.enumeration.DidacticPosition.PROFESSOR;

public class BigBangTest {

    private EmployeeRepository employeeRepository = new EmployeeRepositoryMock();
    private EmployeeController employeeController = new EmployeeController(employeeRepository);

    @Before
    public void setUp() {
        employeeRepository.deleteAll();
    }

    //  LAB 02
    @Test
    public void addEmployeeValidDataTest() {
        Employee employee = new EmployeeBuilder()
                .lastName("Moldovan")
                .firstName("Marian")
                .cnp("1920304231235")
                .didacticPosition(LECTURER)
                .salary(2300.00)
                .build();

        try {
            employeeController.addEmployee(employee);
            List<Employee> employeesList = employeeController.getEmployeesList();
            assertEquals(employeesList.size(), 1);
            Employee actual = employeesList.get(0);
            assertEquals(actual, employee);
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }

    //  LAB 03
    @Test
    public void F02_TC01() {
        try {
            Employee EMPLOYEE_1 = new Employee("Dan", "Alb", "1234567891234", ASSISTANT_PROFESSOR, 5600);
            Employee EMPLOYEE_2 = new Employee("Mihaela", "Suciu", "2234567891234", PROFESSOR, 5600);
            Employee EMPLOYEE_3 = new Employee("Gabriel", "Muresan", "1234567891294", LECTURER, 5600);
            employeeController.addEmployee(EMPLOYEE_1);
            employeeController.addEmployee(EMPLOYEE_2);
            employeeController.addEmployee(EMPLOYEE_3);
        } catch (NonValidEmployeeException e) {
            e.printStackTrace();
        }
        try {
            employeeController.modifyEmployee(null, LECTURER);
            fail();
        } catch (NonValidEmployeeException e) {
            assertEquals(e.getMessage(), "Cnp-ul nu poate fi neprecizat.");
        }
    }

    //  LAB 04
    @Test
    public void testF03_SortByAgeAscending() {
        try {
            employeeController.addEmployee(new Employee("firstName1", "lastName1", "1920603891234", LECTURER, 2.2));
            employeeController.addEmployee(new Employee("firstName2", "lastName2", "1911202891234", ASSISTANT_PROFESSOR, 22.2));
            employeeController.addEmployee(new Employee("firstName3", "lastName3", "1901112123423", ASSOCIATE_PROFESSOR, 122.2));

            List<Employee> employeesSortedByAgeAscending = employeeController.getEmployesSortedByAgeAscending();

            assertEquals(employeesSortedByAgeAscending.size(), 3);
            Employee employee1 = employeesSortedByAgeAscending.get(0);
            assertNotNull(employee1);
            assertEquals(employee1.getFirstName(), "firstName1");

            Employee employee2 = employeesSortedByAgeAscending.get(1);
            assertNotNull(employee2);
            assertEquals(employee2.getFirstName(), "firstName2");

            Employee employee3 = employeesSortedByAgeAscending.get(2);
            assertNotNull(employee3);
            assertEquals(employee3.getFirstName(), "firstName3");
        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testF01_F02_F_03_Integration() {
        try {
            // F01 - ADD
            employeeController.addEmployee(new Employee("firstName1", "lastName1", "1920603891234", LECTURER, 2.2));
            employeeController.addEmployee(new Employee("firstName2", "lastName2", "1911202891234", ASSISTANT_PROFESSOR, 22.2));
            employeeController.addEmployee(new Employee("firstName3", "lastName3", "1901112123423", ASSOCIATE_PROFESSOR, 122.2));

            List<Employee> employeesList = employeeController.getEmployeesList();
            Optional<Employee> employeeOptional1 = employeesList.stream().filter(employee -> employee.getCnp().equals("1920603891234")).findFirst();
            Optional<Employee> employeeOptional2 = employeesList.stream().filter(employee -> employee.getCnp().equals("1911202891234")).findFirst();
            Optional<Employee> employeeOptional3 = employeesList.stream().filter(employee -> employee.getCnp().equals("1901112123423")).findFirst();

            assertTrue(employeeOptional1.isPresent());
            assertTrue(employeeOptional2.isPresent());
            assertTrue(employeeOptional3.isPresent());
            assertEquals(employeeOptional1.get().getFirstName(), "firstName1");
            assertEquals(employeeOptional2.get().getFirstName(), "firstName2");
            assertEquals(employeeOptional3.get().getFirstName(), "firstName3");

            assertEquals(employeeOptional1.get().getLastName(), "lastName1");
            assertEquals(employeeOptional2.get().getLastName(), "lastName2");
            assertEquals(employeeOptional3.get().getLastName(), "lastName3");

            assertEquals(employeeOptional1.get().getPosition(), LECTURER);
            assertEquals(employeeOptional2.get().getPosition(), ASSISTANT_PROFESSOR);
            assertEquals(employeeOptional3.get().getPosition(), ASSOCIATE_PROFESSOR);

            // F02 - Modifica functie angajat
            assertEquals(employeeOptional1.get().getPosition(), LECTURER);
            employeeController.modifyEmployee("1920603891234", ASSOCIATE_PROFESSOR);
            assertEquals(employeeOptional1.get().getPosition(), ASSOCIATE_PROFESSOR);

            // F03 - Sortari
            List<Employee> employesSortedByAgeAscending = employeeController.getEmployesSortedByAgeAscending();
            Employee employee1 = employesSortedByAgeAscending.get(0);
            Employee employee2 = employesSortedByAgeAscending.get(1);
            Employee employee3 = employesSortedByAgeAscending.get(2);

            assertNotNull(employee1);
            assertNotNull(employee2);
            assertNotNull(employee3);
            assertEquals(employee1.getCnp(), "1920603891234");
            assertEquals(employee2.getCnp(), "1911202891234");
            assertEquals(employee3.getCnp(), "1901112123423");

            List<Employee> employesSortedBySalaryDescending = employeeController.getEmployesSortedBySalaryDescending();
            employee1 = employesSortedBySalaryDescending.get(0);
            employee2 = employesSortedBySalaryDescending.get(1);
            employee3 = employesSortedBySalaryDescending.get(2);

            assertNotNull(employee1);
            assertNotNull(employee2);
            assertNotNull(employee3);
            assertEquals(employee1.getCnp(), "1901112123423");
            assertEquals(employee2.getCnp(), "1911202891234");
            assertEquals(employee3.getCnp(), "1920603891234");


        } catch (NonValidEmployeeException e) {
            fail(e.getMessage());
        }
    }
}
