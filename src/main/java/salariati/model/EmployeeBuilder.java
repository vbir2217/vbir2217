package salariati.model;

import salariati.enumeration.DidacticPosition;

public class EmployeeBuilder {
    private Employee employee = new Employee();

    public EmployeeBuilder firstName(String firstName) {
        employee.setFirstName(firstName);
        return this;
    }

    public EmployeeBuilder lastName(String lastName) {
        employee.setLastName(lastName);
        return this;
    }

    public EmployeeBuilder cnp(String cnp) {
        employee.setCnp(cnp);
        return this;
    }

    public EmployeeBuilder didacticPosition(DidacticPosition didacticPosition) {
        employee.setPosition(didacticPosition);
        return this;
    }

    public EmployeeBuilder salary(double salary) {
        employee.setSalary(salary);
        return this;
    }

    public Employee build() {
        return employee;
    }
}
