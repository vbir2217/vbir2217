package salariati.model;

import salariati.enumeration.DidacticPosition;

import java.util.Objects;

import static java.lang.Double.parseDouble;
import static salariati.enumeration.DidacticPosition.ASSISTANT_PROFESSOR;
import static salariati.enumeration.DidacticPosition.ASSOCIATE_PROFESSOR;
import static salariati.enumeration.DidacticPosition.LECTURER;
import static salariati.enumeration.DidacticPosition.PROFESSOR;

public class Employee {

    private String firstName;

    private String lastName;

    private String cnp;

    private DidacticPosition position;

    private double salary;

    public Employee() {
    }

    public Employee(String firstName, String lastName, String cnp, DidacticPosition position, double salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cnp = cnp;
        this.position = position;
        this.salary = salary;
    }

    public static Employee getEmployeeFromString(String _employee) {
        Employee employee = new Employee();

        String[] attributes = _employee.split("[;]");

        employee.setFirstName(attributes[0]);
        employee.setLastName(attributes[1]);
        employee.setCnp(attributes[2]);

        switch (attributes[3]) {
            case "ASSISTANT_PROFESSOR":
                employee.setPosition(ASSISTANT_PROFESSOR);
                break;
            case "PROFESSOR":
                employee.setPosition(PROFESSOR);
                break;
            case "LECTURER":
                employee.setPosition(LECTURER);
                break;
            case "ASSOCIATE_PROFESSOR":
                employee.setPosition(ASSOCIATE_PROFESSOR);
                break;
        }
        employee.setSalary(parseDouble(attributes[4]));

        return employee;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public DidacticPosition getPosition() {
        return position;
    }

    public void setPosition(DidacticPosition position) {
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String toString() {
        return firstName +
                ";" +
                lastName +
                ";" +
                cnp +
                ";" +
                position +
                ";" +
                salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Double.compare(employee.salary, salary) == 0 &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(cnp, employee.cnp) &&
                position == employee.position;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, cnp, position, salary);
    }
}
