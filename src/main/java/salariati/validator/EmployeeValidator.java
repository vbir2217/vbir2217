package salariati.validator;

import salariati.exception.NonValidEmployeeException;
import salariati.model.Employee;

import java.util.ArrayList;
import java.util.List;

import static salariati.enumeration.DidacticPosition.ASSISTANT_PROFESSOR;
import static salariati.enumeration.DidacticPosition.ASSOCIATE_PROFESSOR;
import static salariati.enumeration.DidacticPosition.LECTURER;
import static salariati.enumeration.DidacticPosition.PROFESSOR;

public class EmployeeValidator {

    public EmployeeValidator() {
    }

    public void validate(Employee employee) throws NonValidEmployeeException {
        List<String> errors = new ArrayList<>();
        if (employee.getLastName() == null || employee.getLastName().equals("")) {
            errors.add("Numele nu poate sa fie vid.");
        }
        if (employee.getFirstName() == null || employee.getFirstName().equals("")) {
            errors.add("Prenumele nu poate sa fie vid.");
        }
        if (employee.getLastName().length() > 255) {
            errors.add("Numele poate avea o lungime de maxim 255");
        }
        if (!(employee.getPosition().equals(ASSISTANT_PROFESSOR) ||
                employee.getPosition().equals(ASSOCIATE_PROFESSOR) ||
                employee.getPosition().equals(LECTURER) ||
                employee.getPosition().equals(PROFESSOR))) {
            errors.add("Functia poate sa fie de Asistent Universitar, Lector, Conferentiar sau Profesor Universitar");
        }
        if (!employee.getCnp().matches("[0-9]{13}")) {
            errors.add("CNP-ul trebuie sa fie un numar format din 13 cifre.");
        }
        if (employee.getSalary() <= 0 || employee.getSalary() > 20000) {
            errors.add("Salariul trebuie sa fie un numar pozitiv diferit de 0 si maxim 20000.");
        }
        if (!errors.isEmpty()) {
            throw new NonValidEmployeeException(errors.stream().reduce("", (s1, s2) -> s1 + "\n" + s2));
        }
    }

    public boolean isValid(Employee employee) {
        boolean isLastNameValid = employee.getLastName().matches("[a-zA-Z]+") && (employee.getLastName().length() > 2);
        boolean isCNPValid = employee.getCnp().matches("[a-z0-9]+") && (employee.getCnp().length() == 13);
        boolean isFunctionValid = employee.getPosition().equals(ASSISTANT_PROFESSOR) ||
                employee.getPosition().equals(LECTURER) ||
                employee.getPosition().equals(PROFESSOR);
        boolean isSalaryValid = employee.getSalary() > 0;

        return isLastNameValid && isCNPValid && isFunctionValid && isSalaryValid;
    }

}
