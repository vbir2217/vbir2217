package salariati.exception;

public class NonValidEmployeeException extends Exception {

    private static final long serialVersionUID = 1460943029226542682L;

    public NonValidEmployeeException() {
    }

    public NonValidEmployeeException(String message) {
        super(message);
    }

}
