package salariati.enumeration;

public enum DidacticPosition {
    ASSISTANT_PROFESSOR,
    PROFESSOR,
    LECTURER,
    ASSOCIATE_PROFESSOR
}
