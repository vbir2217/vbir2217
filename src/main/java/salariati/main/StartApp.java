package salariati.main;

import salariati.controller.EmployeeController;
import salariati.repository.implementations.EmployeeRepositoryImpl;
import salariati.repository.interfaces.EmployeeRepository;
import salariati.view.Console;

//functionalitati
//F01.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//F02.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//F03.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).

public class StartApp {

    public static void main(String[] args) {
        final EmployeeRepository employeeRepository = new EmployeeRepositoryImpl();
        final EmployeeController employeeController = new EmployeeController(employeeRepository);
        final Console console = new Console(employeeController);

        console.showMenu();
    }
}
