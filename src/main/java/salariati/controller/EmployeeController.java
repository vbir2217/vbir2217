package salariati.controller;

import salariati.enumeration.DidacticPosition;
import salariati.exception.NonValidEmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepository;
import salariati.validator.EmployeeValidator;

import java.util.List;

public class EmployeeController {

    private EmployeeRepository employeeRepository;
    private EmployeeValidator employeeValidator = new EmployeeValidator();

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public void addEmployee(Employee employee) throws NonValidEmployeeException {
        employeeValidator.validate(employee);
        employeeRepository.addEmployee(employee);
    }

    public void modifyEmployee(String employeeCnp, DidacticPosition newPosition) throws NonValidEmployeeException {
        employeeRepository.modifyPositionOfEmployee(employeeCnp, newPosition);
    }

    public List<Employee> getEmployeesList() {
        return employeeRepository.getEmployeesList();
    }

    public List<Employee> getEmployesSortedBySalaryDescending() {
        return employeeRepository.getEmployeesSortedBySalaryDescending();
    }

    public List<Employee> getEmployesSortedByAgeAscending() {
        return employeeRepository.getEmployeesSortedByAgeAscending();
    }

    public void deleteAll() {
        employeeRepository.deleteAll();
    }
}
