package salariati.view;

import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticPosition;
import salariati.exception.NonValidEmployeeException;
import salariati.model.Employee;

import java.util.List;
import java.util.Scanner;

import static java.lang.System.out;
import static salariati.enumeration.DidacticPosition.ASSISTANT_PROFESSOR;
import static salariati.enumeration.DidacticPosition.ASSOCIATE_PROFESSOR;
import static salariati.enumeration.DidacticPosition.LECTURER;
import static salariati.enumeration.DidacticPosition.PROFESSOR;

public class Console {

    private EmployeeController employeeController;

    public Console(EmployeeController employeeController) {
        this.employeeController = employeeController;
    }

    public void showMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean isFinished = false;
        while (!isFinished) {
            out.println("________________________________________\nAlege o optiune:");
            out.println("1.Adauga un angajat.");
            out.println("2.Modifica functia un angajat.");
            out.println("3.Listare angajati ordonat descrescator dupa salariu.");
            out.println("4.Listare angajati ordonati crescator dupa varsta.");
            out.println("5.Exit.");
            String optiune = scanner.nextLine();
            switch (optiune) {
                case "1":
                    addEmployee(scanner);
                    break;
                case "2":
                    modifyEmployee(scanner);
                    break;
                case "3":
                    showEmployesOrderedBySalaryDescending();
                    break;
                case "4":
                    showEmployesOrderedByAgeAscenidng();
                    break;
                case "5":
                    isFinished = true;
                    break;
            }
        }
    }

    private void addEmployee(Scanner scanner) {
        out.println("Introduceti CNP-ul angajatului (13 cifre):");
        String cnp = scanner.nextLine();
        out.println("Introduceti prenumele angajatului:");
        String prenume = scanner.nextLine();
        out.println("Introduceti numele angajatului:");
        String nume = scanner.nextLine();
        out.println("Introduceti salariul angajatului:");
        try {
            double salariul = scanner.nextDouble();
            scanner.nextLine();
            out.println("Selectati pozitia acestuia:\n1.Asistent Universitar\n2.Lector\n3.Conferentiar\n4.Profesor");
            String pozitiaInt = scanner.nextLine();
            DidacticPosition didacticPosition = getPositionByIndex(pozitiaInt);
            if (didacticPosition != null) {
                try {
                    employeeController.addEmployee(new Employee(prenume, nume, cnp, didacticPosition, salariul));
                } catch (NonValidEmployeeException e) {
                    out.println(e.getMessage());
                }
            } else {
                out.println("Optiunea aleasa este gresita.");
            }
        } catch (NumberFormatException e) {
            out.println("Formatul nu este valid.");
        }
    }

    private void modifyEmployee(Scanner scanner) {
        //  mai intai o sa afisam toti angajatii pentru a avea acces la cnp
        List<Employee> employees = employeeController.getEmployeesList();
        employees.forEach(out::println);

        out.println("Introduceti CNP-ul angajatului:");
        String cnp = scanner.nextLine();
        out.println("Selectati functia dorita:\n1.Asistent Universitar\n2.Lector\n3.Conferentiar\n4.Profesor");
        String pozitiaInt = scanner.nextLine();
        DidacticPosition didacticPosition = getPositionByIndex(pozitiaInt);
        if (didacticPosition != null) {
            try {
                employeeController.modifyEmployee(cnp, didacticPosition);
            } catch (NonValidEmployeeException e) {
                out.println(e.getMessage());
            }
        } else {
            out.println("Optiunea aleasa este gresita.");
        }
    }

    private DidacticPosition getPositionByIndex(String pozitiaInt) {
        switch (pozitiaInt) {
            case "1":
                return ASSISTANT_PROFESSOR;
            case "2":
                return LECTURER;
            case "3":
                return ASSOCIATE_PROFESSOR;
            case "4":
                return PROFESSOR;
            default:
                return null;
        }
    }

    private void showEmployesOrderedBySalaryDescending() {
        List<Employee> employesSortedBySalaryDescending = employeeController.getEmployesSortedBySalaryDescending();
        employesSortedBySalaryDescending.forEach(out::println);
    }

    private void showEmployesOrderedByAgeAscenidng() {
        List<Employee> employesSortedByAgeAscending = employeeController.getEmployesSortedByAgeAscending();
        employesSortedByAgeAscending.forEach(out::println);
    }


}
