package salariati.repository.mock;

import salariati.enumeration.DidacticPosition;
import salariati.exception.NonValidEmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toList;

@SuppressWarnings("Duplicates")
public class EmployeeRepositoryMock implements EmployeeRepository {

    private Map<String, Employee> employees = new HashMap<>();

    @Override
    public void addEmployee(Employee employee) {
        employees.put(employee.getCnp(), employee);
    }

    @Override
    public void modifyPositionOfEmployee(String cnpAngajat, DidacticPosition functiaNoua) throws NonValidEmployeeException {
        List<Employee> angajati = new ArrayList<>(employees.values());
        if ((cnpAngajat == null) || (cnpAngajat.equals(""))) {
            throw new NonValidEmployeeException("Cnp-ul nu poate fi neprecizat.");
        } else if (functiaNoua == null) {
            throw new NonValidEmployeeException("Functia didactica trebuie introdusa.");
        } else {
            int index = 0;
            while (index < angajati.size()) {
                Employee angajatCurent = angajati.get(index);
                if (angajatCurent.getCnp().equals(cnpAngajat)) {
                    angajatCurent.setPosition(functiaNoua);
                    return;
                }
                index++;
            }
        }
    }

    @Override
    public List<Employee> getEmployeesList() {
        return new ArrayList<>(employees.values());
    }

    @Override
    public List<Employee> getEmployeesSortedBySalaryDescending() {
        return employees.values()
                .stream()
                .sorted((o1, o2) -> {
                    double diff = o1.getSalary() - o2.getSalary();
                    if (diff == 0) {
                        return 0;
                    } else return (diff < 0) ? 1 : -1;
                })
                .collect(toList());
    }

    @Override
    public List<Employee> getEmployeesSortedByAgeAscending() {
        return employees.values()
                .stream()
                .sorted((o1, o2) -> {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    //  ne ocupam de o1
                    int deceniul1 = (parseInt(valueOf(o1.getCnp().charAt(0))));
                    int anulNasterii1 = (deceniul1 == 1 || deceniul1 == 2) ?
                            parseInt("19" + o1.getCnp().substring(1, 3)) :
                            parseInt("20" + o1.getCnp().substring(1, 3));
                    int luna1 = o1.getCnp().charAt(3) == '0' ?
                            parseInt(valueOf(o1.getCnp().charAt(4))) :
                            parseInt(o1.getCnp().substring(3, 5));
                    int zi1 = o1.getCnp().charAt(5) == '0' ?
                            parseInt(valueOf(o1.getCnp().charAt(6))) :
                            parseInt(o1.getCnp().substring(5, 7));
                    Date date1 = new Date();
                    try {
                        date1 = sdf.parse(anulNasterii1 + "-" + luna1 + "-" + zi1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    int deceniul2 = (parseInt(valueOf(o2.getCnp().charAt(0))));
                    int anulNasterii2 = (deceniul2 == 1 || deceniul2 == 2) ?
                            parseInt("19" + o2.getCnp().substring(1, 3)) :
                            parseInt("20" + o2.getCnp().substring(1, 3));
                    int luna2 = o2.getCnp().charAt(3) == '0' ?
                            parseInt(valueOf(o2.getCnp().charAt(4))) :
                            parseInt(o2.getCnp().substring(3, 5));
                    int zi2 = o2.getCnp().charAt(5) == '0' ?
                            parseInt(valueOf(o2.getCnp().charAt(6))) :
                            parseInt(o2.getCnp().substring(5, 7));
                    Date date2 = new Date();
                    try {
                        date2 = sdf.parse(anulNasterii2 + "-" + luna2 + "-" + zi2);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    return date2.compareTo(date1);
                })
                .collect(toList());
    }

    @Override
    public void deleteAll() {
        employees.clear();
    }

    @Override
    public void modificaFunctieAnagajat(String cnpAngajat, DidacticPosition functiaNoua) throws NonValidEmployeeException {
        List<Employee> angajati = new ArrayList<>(employees.values());
        if ((cnpAngajat == null) || (cnpAngajat.equals(""))) {
            throw new NonValidEmployeeException("Cnp-ul nu poate fi neprecizat.");
        } else if (functiaNoua == null) {
            throw new NonValidEmployeeException("Functia didactica trebuie introdusa.");
        } else {
            int index = 0;
            while (index < angajati.size()) {
                Employee angajatCurent = angajati.get(index);
                if (angajatCurent.getCnp().equals(cnpAngajat)) {
                    angajatCurent.setPosition(functiaNoua);
                    return;
                }
                index++;
            }
        }
    }

    @Override
    public Optional<Employee> getEmployeeByCnp(String cnp) {
        if (cnp == null || cnp.equals("")) {
            throw new NullPointerException("CNP-ul nu poate fi null.");
        }
        return (employees.get(cnp) != null) ? of(employees.get(cnp)) : empty();
    }
}
