package salariati.repository.interfaces;

import salariati.enumeration.DidacticPosition;
import salariati.exception.NonValidEmployeeException;
import salariati.model.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {

    void addEmployee(Employee employee) throws NonValidEmployeeException;

    void modifyPositionOfEmployee(String employeeCnp, DidacticPosition newPosition) throws NonValidEmployeeException;

    List<Employee> getEmployeesList();

    List<Employee> getEmployeesSortedBySalaryDescending();

    List<Employee> getEmployeesSortedByAgeAscending();

    void deleteAll();

    void modificaFunctieAnagajat(String cnpAngajat, DidacticPosition functiaAngajatului) throws NonValidEmployeeException;

    Optional<Employee> getEmployeeByCnp(String cnp);
}
